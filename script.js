const books = [
    { 
        author: "Люсі Фолі",
        name: "Список запрошених",
        price: 70 
    }, 
    {
        author: "Сюзанна Кларк",
        name: "Джонатан Стрейндж і м-р Норрелл",
    }, 
    { 
        name: "Дизайн. Книга для недизайнерів.",
        price: 70
    }, 
    { 
        author: "Алан Мур",
        name: "Неономікон",
        price: 70
    }, 
    {
        author: "Террі Пратчетт",
        name: "Рухомі картинки",
        price: 40
    },
    {
        author: "Анґус Гайленд",
        name: "Коти в мистецтві",
    }
];

function isValidBook(book) {
    const requiredProperties = ['author', 'name', 'price'];
    for (const prop of requiredProperties) {
        if (!book.hasOwnProperty(prop)) {
            throw new Error(`Помилка: Відсутня властивість "${prop}" в book`);
        }
    }
    return true;
}

function createBookList(books) {
    const ul = document.createElement('ul');
    
    books.forEach(book => {
        try {
            if (isValidBook(book)) {
                const li = document.createElement('li');
                li.textContent = `${book.name} by ${book.author}, Price: ${book.price}`;
                ul.appendChild(li);
            }
        } catch (error) {
            console.error(error.message, book);
        }
    });

    return ul;
}

const root = document.getElementById('root');
const bookList = createBookList(books);
root.appendChild(bookList);
